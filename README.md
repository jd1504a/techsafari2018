### TechSafari2018 ###
For AU Tech Safari 2018, we are tasked to create a data visualization website/presentation. The first task at hand is to create
the hardware that will collect the data. After some discussions we deciced upon GPS data and environmental data. The device is able
to take data at set increments that is choosen by potentiometer.

### Hardware Side ###
The data collection device G-PATH (Gps, Pressure, Altitude, Tempature, Humidity).\n
The device should be Set-and-Forget.\n
Make sure the battery is charged, once plugged in the device will run until the battery dies.

### Prerequistes ###
None

### Built With ###
	-Teensyduino, A plug in for Arduino
	-Adafruit Ultimite GPS Breakout
	-Adafruit SD Breakout
	-Adafruit BME680 (Environmental Sensor)
	
### Software Side ###

### Prerequistes ###

### Built With ###

### Authors ###
James Deromedi
Yan
Cedric
Dong

### License ###
This is a free-ware at the moment. Please just reconize the author(s) if you use anything posted here

### Acknowledgements ###
Kristof for being our advisor\n
John for being the amazing sponsor and setting up this whole trip and event
